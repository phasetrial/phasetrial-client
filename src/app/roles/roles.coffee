angular.module 'phasetrial'
# SchemaForm: form array
.factory 'RoleForm', (RightsMap) ->
  return [
    'name'
    {key: 'rights', htmlClass: 'checkbox-primary', type: 'checkboxes', titleMap: RightsMap}
  ]
# SchemaForm: schema object
.factory 'RoleSchema', () ->
  return {
    type: 'object'
    title: 'Role'
    properties:
      name:
        type: 'string'
        title: 'Name'
      rights:
        type: 'array'
        title: 'Rights'
        items:
          type: 'string'
  }
# Column definitions for UI Grid.
.factory 'RoleColumns', (uiGridConstants) ->
  return [
    {field: 'id', displayName: '', width: 32, cellTemplate: 'app/common/edit-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {field: 'name', width: 200, sort: { direction: uiGridConstants.ASC }, priority: 2}
    {field: 'rights', cellTemplate: 'app/roles/rights-cell.html'}
    {name: 'Delete', displayName: '', width: 32, cellTemplate: 'app/common/delete-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
  ]
# Resource for getting roles table from database.
.factory 'RoleResource', (railsResourceFactory) ->
  return railsResourceFactory {
    url: "/api/roles"
    name: "role"
  }
# Controller for adding new roles.
.controller 'AddRoleCtrl', ($scope, RoleSchema, RoleForm, RoleResource, $uibModalInstance) ->
  $scope.model = {}
  $scope.schema = RoleSchema
  $scope.form = RoleForm
  $scope.save = ->
    new RoleResource($scope.model).create()
    $uibModalInstance.close $scope.model
# Controller for editing existing roles.
.controller 'EditRoleCtrl', ($scope, RoleSchema, RoleForm, grid, row, RoleResource, $uibModalInstance) ->
  $scope.schema = RoleSchema
  $scope.form = RoleForm
  $scope.grid = grid
  $scope.row = row
  $scope.model = angular.copy row.entity
  $scope.save = ->
    row.entity = angular.extend row.entity, $scope.model
    RoleResource.get(row.entity.id).then (resrc) ->
      resrc = angular.extend row.entity
      resrc.update()
    $uibModalInstance.close row.entity
# Controller for deleting existing roles
.controller 'DelRoleCtrl', ($scope, RoleSchema, row, RoleResource, $uibModalInstance) ->
  $scope.schema = RoleSchema
  $scope.name = row.entity.name
  $scope.delete = ->
    RoleResource.get(row.entity.id).then (resrc) -> resrc.delete()
    $uibModalInstance.close row.entity
# Base controller. Loads roles into the grid and contains button functions.
.controller 'RolesCtrl', ($scope, RoleColumns, $uibModal, RoleResource, gridFactory, RightsMap, $window) ->
  $scope.addRow = () ->
    modal = $uibModal.open
      templateUrl: 'app/common/add-row-modal.html'
      controller: 'AddRoleCtrl'
    modal.result.then (result) ->
      $scope.gridOptions.data.push result unless result is undefined
  $scope.editRow = (grid, row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/edit-row-modal.html'
      controller: 'EditRoleCtrl'
      resolve:
        grid: ->
          grid
        row: ->
          row
  $scope.delRow = (row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/delete-row-modal.html'
      controller: 'DelRoleCtrl'
      resolve:
        row: ->
          row
    modal.result.then (result) ->
      index = $scope.gridOptions.data.indexOf(result.data)
      $scope.gridOptions.data.splice index, 1
  $scope.rightsFormatter = (rights) ->
    rightsString = ''
    angular.forEach rights, (right) ->
      rightObj = RightsMap.filter (obj) ->
        return obj.value is right
      rightsString += rightObj[0].name + ', '
    return rightsString.substring(0, rightsString.length - 2)
  $scope.gridOptions = gridFactory.build($scope, RoleColumns, false, false)
  $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  angular.element($window).bind 'resize', ->
    $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  RoleResource.query().then (data) -> $scope.gridOptions.data = data
