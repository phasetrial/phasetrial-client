angular.module 'phasetrial'
# Controller for new user registration
.controller 'ForgotCtrl', ($scope, $auth) ->
  $scope.forgotForm = {}
  $scope.submit = () ->
    $scope.success = false
    $scope.error = false
    $auth.requestPasswordReset($scope.forgotForm)
  $scope.$on 'auth:password-reset-request-success', (ev, message) ->
    $scope.success = 'Check your email to reset your password.'
  $scope.$on 'auth:password-reset-request-error', (ev, reason) ->
    $scope.error = reason.errors[0]
