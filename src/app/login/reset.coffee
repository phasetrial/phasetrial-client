angular.module 'phasetrial'
# Controller for new user registration
.controller 'ResetCtrl', ($scope, $auth, $state, $timeout) ->
  $scope.resetForm = {}
  $scope.submit = () ->
    $scope.success = false
    $scope.error = false
    $auth.updatePassword($scope.resetForm)
  $scope.$on 'auth:password-change-success', (ev, message) ->
    $scope.success = 'Password reset successful!'
    $timeout((-> $state.go 'home'), 2000)
  $scope.$on 'auth:password-change-error', (ev, reason) ->
    $scope.error = reason.errors.full_messages[0]
