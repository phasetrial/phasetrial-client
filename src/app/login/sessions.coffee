angular.module 'phasetrial'
# Controller for login page.
.controller 'SessionsCtrl', ($scope, $auth, UserResource, Useful, $state) ->
  $scope.loginForm = {}
  $scope.submit = () ->
    UserResource.$get('/api/users/find', {uid: $scope.loginForm.email}).then (user) ->
      if user.locked then $scope.error = 'User account locked.' else $auth.submitLogin($scope.loginForm).then (resp) ->
        $scope.$parent.user = resp
        $state.go 'home'
  $scope.$on 'auth:login-error', (ev, reason) ->
    $scope.error = reason.errors[0]
  if Useful.getParameterByName('account_confirmation_success') is 'true'
    $scope.loginForm.email = Useful.getParameterByName('uid')
    $scope.confirm = "Account confirmed. Please login." if Useful.getParameterByName('locked') is 'false'
    $scope.confirm = "Account confirmed. Please contact administrator to unlock account." if Useful.getParameterByName('locked') is 'true'
