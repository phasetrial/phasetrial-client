angular.module 'phasetrial'
# Column definitions for UI Grid.
.factory 'SubjectViewColumns', (uiGridConstants) ->
  return [
    {field: 'seq', sort: { direction: uiGridConstants.ASC }, priority: 2, width: 40, visible: false}
    {name: 'Open', displayName: '', width: 32, cellTemplate: 'app/common/open-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {field: 'name'}
  ]
# Resource for getting forms table from database.
.factory 'FormResource', (railsResourceFactory, railsSerializer, RailsInflector) ->
  return railsResourceFactory {
    url: "/api/forms"
    name: "form"
    serializer: railsSerializer () ->
      this.serializeWith 'data', railsSerializer {underscore: RailsInflector.camelize}
  }
# Controller for opening log forms.
.controller 'OpenLogFormCtrl', ($scope, $uibModal, FormResource, apprId, formType, subject, form, gridFactory) ->
  $scope.subject = subject
  $scope.curForm = form
  $scope.title = formType.name
  $scope.variables = formType.variableAppearances
  $scope.columns = [
    {name: 'seq', displayName: '#', width: 20, cellTemplate: '<div class="ui-grid-cell-contents">{{grid.rows.indexOf(row) + 1}}</div>', headerCellTemplate: 'app/common/header-cell-no-sort.html'}
    {name: 'Open', displayName: '', width: 32, cellTemplate: 'app/common/open-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
  ]
  angular.forEach $scope.variables, (variable) ->
    $scope.columns.push
      field: variable.varname
      displayName: variable.label
  $scope.gridOptions = gridFactory.build($scope, $scope.columns, false, false)
  $scope.gridOptions.data = form.data.log
  $scope.open = (row) ->
    secondModal = $uibModal.open
      templateUrl: 'app/common/open-form-modal.html'
      controller: 'OpenFormCtrl'
      resolve:
        apprId: ->
          apprId
        formType: ->
          formType
        subject: ->
          $scope.subject
        form: ->
          { data: row.entity, logForm: true }
    secondModal.result.then () ->
      $scope.curForm.update()
  $scope.addRow = ->
    secondModal = $uibModal.open
      templateUrl: 'app/common/open-form-modal.html'
      controller: 'OpenFormCtrl'
      resolve:
        apprId: ->
          apprId
        formType: ->
          formType
        subject: ->
          $scope.subject
        form: ->
          { data: {}, logForm: true }
    secondModal.result.then (result) ->
      if result
        $scope.curForm.data.log.push result.data
        $scope.curForm.update()
#      else if result and !$scope.curForm.id?
#        $scope.curForm.data.log.push result.data
#        $scope.curForm.create()
# Controller for opening forms in a subject's casebook.
.controller 'OpenFormCtrl', ($scope, FormResource, $uibModalInstance, apprId, formType, subject, form) ->
  # Initial setup
  $scope.preview = false
  $scope.subject = subject
  $scope.curForm = form
  $scope.model = form.data
  $scope.title = formType.name
  $scope.variables = formType.variableAppearances.sort (a, b) -> return a.seq - b.seq
  $scope.schema = {
    type: 'object'
    title: $scope.title
    properties: {}
  }
  $scope.form = []
  # Save function updates existing form or creates a new form
  $scope.save = ->
    $scope.curForm.data = $scope.model
    if $scope.curForm.logForm
      $uibModalInstance.close $scope.curForm
    else if $scope.curForm.id?
      $scope.curForm.update()
      $uibModalInstance.close $scope.curForm
    else
      $scope.curForm.subjectId = $scope.subject.id
      $scope.curForm.formAppearanceId = apprId
      newForm = new FormResource($scope.curForm).create()
      $uibModalInstance.close newForm
  # Schema object and form array setup based on variable type, including date/time validation
  angular.forEach $scope.variables, (variable) ->
    $scope.schema.properties[variable.varname] =
      type: switch variable.datatype
        when 'integer', 'float' then 'number'
        when 'text', 'radio', 'datetime' then 'string'
        when 'checkbox' then 'array'
      title: variable.label
    if variable.datatype is 'radio'
      $scope.schema.properties[variable.varname].enum = variable.options
    if variable.datatype is 'checkbox'
      $scope.schema.properties[variable.varname].items = { type: 'string', enum: variable.options }
    formObj = { key: variable.varname }
    if variable.datatype is 'datetime'
      formObj.type = 'datepicker'
      formObj.format = 'MMMM dd, yyyy'
      if variable.options[0] is 'date' or variable.options[1] is 'date'
        formObj.date = true
      if variable.options[0] is 'time' or variable.options[1] is 'time'
        formObj.time = true
      if $scope.model[variable.varname] is undefined
        formObj.inputDate = null
        formObj.inputTime = ""
        formObj.manualDate = ""
      else
        if $scope.model[variable.varname].match(/(^|\s)([0-9]|0[1-9]|[1-2][0-9]|3[0-1])(\s|,|$)/) is null
          formObj.inputDate = $scope.model[variable.varname].replace(/([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/, '$`')
        else
          formObj.inputDate = new Date($scope.model[variable.varname])
        if $scope.model[variable.varname].match(/([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/) is null
          formObj.inputTime = ""
        else
          formObj.inputTime = $scope.model[variable.varname].match(/([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/)[0]
      formObj.$validators =
        invalidDate: (value) ->
          if formObj.date?
            if formObj.manualDate is "" then return true
            if !isNaN(Date.parse(value)) and Date.parse(value) >= -2208988800000 then return true
            else return false
          else return true
        invalidTime: (value) ->
          if formObj.time?
            if formObj.inputTime is "" then return true
            if formObj.inputTime.match(/(^[0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/) is null then return false
          return true
      formObj.opened = false
      formObj.open = () ->
        formObj.opened = !formObj.opened
      formObj.notEmpty = () ->
        if formObj.manualDate is "" and formObj.inputTime is "" then return false
        return true
    if variable.datatype is 'checkbox'
      formObj.htmlClass = 'checkbox-primary'
    $scope.form.push formObj
# Base controller. Loads visits and forms into the grid and contains button functions.
.controller 'SubjectViewCtrl', ($scope, $stateParams, SubjectViewColumns, VisitResource, FormTypeResource, SiteResource, SubjectResource, FormResource, gridFactory, $window, $uibModal) ->
  $scope.nums = $stateParams
  SiteResource.$get('/api/sites/find', {number: $stateParams.siteNum}).then (site) ->
    SubjectResource.$get('/api/subjects/find', {number: $stateParams.subNum, siteId: site[0].id}).then (subject) ->
      $scope.subject = subject[0]
  $scope.open = (row) ->
    if row.entity.log
      modal = $uibModal.open
        templateUrl: 'app/common/log-form-modal.html'
        controller: 'OpenLogFormCtrl'
        size: 'lg'
        resolve:
          apprId: ->
            row.entity.id
          formType: ->
            FormTypeResource.get(row.entity.formTypeId).then (data) -> return data
          subject: ->
            $scope.subject
          form: ->
            FormResource.$get('/api/forms/find', {subjectId: $scope.subject.id, formAppearanceId: row.entity.id}).then (resp) ->
              if resp.length is 1
                return resp[0]
              else
                return new FormResource({subjectId: $scope.subject.id, formAppearanceId: row.entity.id, data: {log: []}}).create()
    else
      modal = $uibModal.open
        templateUrl: 'app/common/open-form-modal.html'
        controller: 'OpenFormCtrl'
        resolve:
          apprId: ->
            row.entity.id
          formType: ->
            FormTypeResource.get(row.entity.formTypeId).then (data) -> return data
          subject: ->
            $scope.subject
          form: ->
            FormResource.$get('/api/forms/find', {subjectId: $scope.subject.id, formAppearanceId: row.entity.id}).then (resp) ->
              if resp.length is 1
                return resp[0]
              else
                return {data: {}}
  VisitResource.query().then (data) ->
    angular.forEach data, (visit) ->
      visit.$$treeLevel = 0
    $scope.gridOptions.data = data
  $scope.gridOptions = gridFactory.build($scope, SubjectViewColumns, false, true)
  $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  angular.element($window).bind 'resize', ->
    $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  
