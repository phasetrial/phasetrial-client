angular.module 'phasetrial'
# SchemaForm: schema object
.constant 'SubjectSchema', {
  type: 'object'
  title: 'Subject'
  properties:
    number:
      type: 'string'
      title: 'Number'
    siteId:
      type: 'number'
      title: 'Site'
    status:
      type: 'string'
      title: 'Status'
      enum: ['Screening', 'Screen Failure', 'Enrolled', 'Randomized', 'Open Label', 'Discontinued Treatment']
  }
# Column definitions for UI Grid.
.factory 'SubjectColumns', (uiGridConstants) ->
  return [
    {field: 'id', displayName: '', width: 32, cellTemplate: 'app/common/edit-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {name: 'Go', displayName: '', width: 32, cellTemplate: 'app/common/go-to-subject-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {field: 'number', width: 120}
    {field: 'siteNumber', displayName: 'Site Number', width: 120}
    {field: 'status', name: 'Status'}
    {name: 'Delete', displayName: '', width: 32, cellTemplate: 'app/common/delete-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
  ]
# Resource for getting subjects table from database.
.factory 'SubjectResource', (railsResourceFactory) ->
  return railsResourceFactory {
    url: "/api/subjects"
    name: "subject"
  }
# Factory to get existing sites as options for a new subject.
.factory 'SiteMap', (SiteResource) ->
  return {
    getSites: (titleMap) ->
      SiteResource.query().then (data) ->
        angular.forEach data, (site) ->
          titleMap.push { value: site.id, name: site.number }
      return titleMap
  }
# Controller for adding new subjects.
.controller 'AddSubjectCtrl', ($scope, SubjectSchema, SiteMap, SubjectResource, $uibModalInstance) ->
  $scope.model = {}
  $scope.schema = SubjectSchema
  $scope.form = [
    'number'
    {key: 'siteId', type: 'select', titleMap: SiteMap.getSites([])}
    'status'
  ]
  $scope.save = ->
    new SubjectResource($scope.model).create()
    $uibModalInstance.close $scope.model
# Controller for editing existing subjects.
.controller 'EditSubjectCtrl', ($scope, SubjectSchema, SiteMap, grid, row, SubjectResource, $uibModalInstance) ->
  $scope.schema = SubjectSchema
  $scope.form = [
    { key: 'number', readonly: true}
    {key: 'siteId', type: 'select', titleMap: SiteMap.getSites([]), readonly: true}
    'status'
  ]
  $scope.grid = grid
  $scope.row = row
  $scope.model = angular.copy row.entity
  $scope.save = ->
    row.entity = angular.extend row.entity, $scope.model
    SubjectResource.get(row.entity.id).then (resrc) ->
      resrc = angular.extend row.entity
      resrc.update()
    $uibModalInstance.close row.entity
# Controller for deleting existing subjects.
.controller 'DelSubjectCtrl', ($scope, SubjectSchema, row, SubjectResource, $uibModalInstance) ->
  $scope.schema = SubjectSchema
  $scope.name = row.entity.name
  $scope.delete = ->
    SubjectResource.get(row.entity.id).then (resrc) -> resrc.delete()
    $uibModalInstance.close row.entity
# Base controller. Loads subjects into the grid and contains button functions.
.controller 'SubjectsCtrl', ($scope, SubjectColumns, $uibModal, SubjectResource, gridFactory, $window) ->
  $scope.addRow = () ->
    modal = $uibModal.open
      templateUrl: 'app/common/add-row-modal.html'
      controller: 'AddSubjectCtrl'
    modal.result.then (result) ->
      $scope.gridOptions.data.push result unless result is undefined
  $scope.editRow = (grid, row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/edit-row-modal.html'
      controller: 'EditSubjectCtrl'
      resolve:
        grid: ->
          grid
        row: ->
          row
  $scope.delRow = (row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/delete-row-modal.html'
      controller: 'DelSubjectCtrl'
      resolve:
        row: ->
          row
    modal.result.then (result) ->
      index = $scope.gridOptions.data.indexOf(result.data)
      $scope.gridOptions.data.splice index, 1
  $scope.gridOptions = gridFactory.build($scope, SubjectColumns)
  $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  angular.element($window).bind 'resize', ->
    $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  SubjectResource.query().then (data) -> $scope.gridOptions.data = data
