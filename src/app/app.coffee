'use strict'
##
# Many thanks to Brian Hann: http://brianhann.com/create-a-modal-row-editor-for-ui-grid-in-minutes/
##
angular.module('phasetrial', [
  'ngAnimate'                 # Angular module for CSS animations.                          https://docs.angularjs.org/api/ngAnimate
  'ngCookies'                 # Angular module for reading and writing browser cookies.     https://docs.angularjs.org/api/ngCookies
  'ngTouch'                   # Angular module for touch events on touchscreen devices.     https://docs.angularjs.org/api/ngTouch
  'ngSanitize'                # Angular module for sanitizing HTML.                         https://docs.angularjs.org/api/ngSanitize
  'ui.router'                 # Flexible routing with nested views in AngularJS.            http://angular-ui.github.io/ui-router/site
  'permission'                # Simple route authorization via roles/permissions            https://github.com/Narzerus/angular-permission
  'permission.ui'             # UI Router plugin for permissions                            https://github.com/Narzerus/angular-permission/blob/development/docs/ui-router/2-usage-in-states.md
  'ui.bootstrap'              # Bootstrap components written in pure AngularJS.             https://angular-ui.github.io/bootstrap/
  'ui.sortable'               # Allows you to sort an array with drag & drop.               https://github.com/angular-ui/ui-sortable
  'ui.select'                 # AngularJS-native version of Select2 and Selectize.          http://angular-ui.github.io/ui-select/
  'ui.grid'                   # A data grid for AngularJS.                                  http://ui-grid.info/
  'ui.grid.resizeColumns'     # UI Grid - allows resizable columns.                         http://ui-grid.info/docs/#/tutorial/204_column_resizing
  'ui.grid.autoResize'        # UI Grid - automatically resizes the grid's width.           http://ui-grid.info/docs/#/tutorial/213_auto_resizing
  'ui.grid.draggable-rows'    # UI Grid - allows rows to be dragged and sorted.             https://github.com/cdwv/ui-grid-draggable-rows
  'ui.grid.treeBase'          # UI Grid - contains the base for treeView.                   http://ui-grid.info/docs/#/tutorial/215_treeView
  'ui.grid.treeView'          # UI Grid - allows rows to act as nodes with children.        http://ui-grid.info/docs/#/tutorial/215_treeView
  'rails'                     # Rails resource factory.                                     https://github.com/FineLinePrototyping/angularjs-rails-resource
  'schemaForm'                # Generate forms from JSON schemas using AngularJS!           https://github.com/json-schema-form/angular-schema-form
  'ng-token-auth'             # Simple, secure authentication for AngularJS.                https://github.com/lynndylanhurley/ng-token-auth
])
# Constant containing application rights
.constant 'RightsMap', [
  {value: 'userAdmin', name: 'User Administration'}
  {value: 'roleAdmin' , name: 'Role Administration'}
  {value: 'siteAdmin' , name: 'Site Administration'}
  {value: 'dev' , name: 'Development'}
  {value: 'test' , name: 'Testing'}
  {value: 'dataEntry' , name: 'Data Entry'}
  {value: 'dataReview' , name: 'Data Review'}
  {value: 'dataManagement' , name: 'Data Management'}
  {value: 'query', name: 'Queries'}
  {value: 'sign' , name: 'Signatures'}
]
# Set up PermissionStore
.run (RightsMap, PermissionStore, $rootScope, $auth) ->
  rights = []
  angular.forEach RightsMap, (right) ->
    rights.push right.value
  PermissionStore.defineManyPermissions rights, (rightName) ->
    if $rootScope.user.rights is undefined then $auth.validateUser().then (resp) ->
      $rootScope.user = resp
      return $rootScope.user.rights.indexOf(rightName) > -1
    else return $rootScope.user.rights.indexOf(rightName) > -1
# Application configuration
.config ($stateProvider, $urlRouterProvider, $authProvider, $compileProvider) ->
# UI Router setup
  $stateProvider
# Main states
    .state 'auth',
      abstract: true
      url: '/'
      templateUrl: 'app/common/content.html'
      resolve:
        auth: ($auth) ->
          $auth.validateUser()
    .state 'home',
      parent: 'auth'
      url: 'home'
      templateUrl: 'app/home/home.html'
    .state 'minor',
      parent: 'auth'
      url: 'minor'
      templateUrl: 'app/home/minor.html'
    .state 'config',
      parent: 'auth'
      url: 'config'
      templateUrl: 'app/home/config.html'
      controller: 'ConfigCtrl'
    .state 'changePass',
      parent: 'auth'
      url: 'change-password'
      templateUrl: 'app/home/change-pass.html'
      controller: 'ChangePassCtrl'
# Login states
    .state 'login',
      url: '/login'
      templateUrl: 'app/login/login.html'
      controller: 'SessionsCtrl'
    .state 'forgot',
      url: '/forgot'
      templateUrl: 'app/login/forgot.html'
      controller: 'ForgotCtrl'
    .state 'reset',
      url: '/reset'
      templateUrl: 'app/login/reset.html'
      controller: 'ResetCtrl'
# Environment states
    .state 'admin',
      parent: 'auth'
      url: 'admin'
      template: '<ui-view/>'
      data:
        permissions:
          only: ['userAdmin','roleAdmin','siteAdmin']
          redirectTo: 'home'
    .state 'dev',
      parent: 'auth'
      url: 'dev'
      template: '<ui-view/>'
      data:
        permissions:
          only: 'dev'
          redirectTo: 'home'
    .state 'test',
      parent: 'auth'
      url: 'test'
      template: '<ui-view/>'
      data:
        permissions:
          only: 'test'
          redirectTo: 'home'
    .state 'stage',
      parent: 'auth'
      url: 'stage'
      template: '<ui-view/>'
      data:
        permissions:
          only: 'test'
          redirectTo: 'home'
    .state 'live',
      parent: 'auth'
      url: 'live'
      template: '<ui-view/>'
      data:
        permissions:
          only: ['dataEntry','dataReview','dataManagement','query','sign']
          redirectTo: 'home'
# Admin states
    .state 'admin.users',
      url: '/users'
      templateUrl: 'app/users/users.html'
      controller: 'UsersCtrl'
      data:
        permissions:
          only: 'userAdmin'
          redirectTo: 'home'
    .state 'admin.roles',
      url: '/roles'
      templateUrl: 'app/roles/roles.html'
      controller: 'RolesCtrl'
      data:
        permissions:
          only: 'roleAdmin'
          redirectTo: 'home'
    .state 'admin.sites',
      url: '/sites'
      templateUrl: 'app/sites/sites.html'
      controller: 'SitesCtrl'
      data:
        permissions:
          only: 'siteAdmin'
          redirectTo: 'home'
# Dev states
    .state 'dev.visits',
      url: '/visits'
      templateUrl: 'app/visits/visits.html'
      controller: 'VisitsCtrl'
    .state 'dev.forms',
      url: '/forms'
      templateUrl: 'app/forms/forms.html'
      controller: 'FormsCtrl'
    .state 'dev.vars',
      url: '/vars'
      templateUrl: 'app/variables/variables.html'
      controller: 'VarsCtrl'
# Test states
    .state 'test.subjects',
      url: '/subjects'
      templateUrl: 'app/subjects/subjects.html'
      controller: 'SubjectsCtrl'
    .state 'test.subject',
      url: '/subject/:siteNum/:subNum'
      controller: 'SubjectViewCtrl'
      templateUrl: 'app/subjects/subject-view.html'
    .state 'test.queries',
      url: '/queries'
      templateUrl: 'app/queries/queries.html'
      controller: 'QueriesCtrl'
    .state 'test.signatures',
      url: '/signatures'
      templateUrl: 'app/signatures/signatures.html'
      controller: 'SigsCtrl'
    .state 'test.reports',
      url: '/reports'
      templateUrl: 'app/reports/reports.html'
      controller: 'ReportsCtrl'
  $urlRouterProvider.otherwise ($injector) ->
    $state = $injector.get '$state'
    $state.go 'home'
# ng-token-auth configuration
  $authProvider.configure
    confirmationSuccessUrl: "https://" + location.host + "/#/login"
    passwordResetSuccessUrl: "https://" + location.host + "/#/reset"
# Disable debug info in production
  $compileProvider.debugInfoEnabled(false) if location.host.match(/phasetrial/)
.run ($rootScope, $timeout, $state, $location, $auth) ->
# Upgrade elements for Material Design Lite. https://www.getmdl.io/components/index.html
  $rootScope.$on '$viewContentLoaded', () ->
    $timeout () ->
      componentHandler.upgradeAllRegistered()
      $auth.validateUser()
# Don't allow logged out users to access any states other than login and reset
  $rootScope.$on 'auth:logout-success', () ->
    $state.go 'login'
  $rootScope.$on 'auth:invalid', () ->
    $location.path '/login' unless $location.path() is '/forgot' or $location.path() is '/reset'
# Factory to provide useful functions.
.factory 'Useful', () ->
  return {
    # Thanks Nimphious: http://stackoverflow.com/questions/10726909/random-alpha-numeric-string-in-javascript
    randPass: (len) ->
      result = ''
      chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#$%?&*'
      result += chars[Math.floor(Math.random() * chars.length)] while result.length < len
      return result
    # Thanks jolly.exe: http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
    getParameterByName: (name, url) ->
      url = window.location.href if !url
      name = name.replace(/[\[\]]/g, "\\$&")
      regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i")
      results = regex.exec(url)
      return null if !results
      return '' if !results[2]
      return decodeURIComponent(results[2].replace(/\+/g, " "))
  }
# Service for setting intercept URL for backend requests
# Thanks uberspeck: https://github.com/FineLinePrototyping/angularjs-rails-resource/issues/161
.service "api", ($location) ->
  if (/localhost/).test $location.host()
    "baseUrl": "http://localhost:9000"
  else if $location.host() is 'dev.clyde.cafe'
    "baseUrl": "https://dev.clyde.cafe"
  else
    "baseUrl": "https://" + $location.host().substring(0, $location.host().indexOf('.')) + ".rails.phasetrial.com"
.config ($httpProvider) ->
  $httpProvider.interceptors.push ($q, api) ->
    "request": (config) ->
      config.url = api.baseUrl + config.url if config.url.match(/^\/api/)
      config || $q.when(config)
