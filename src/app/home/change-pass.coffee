angular.module('phasetrial')
.controller 'ChangePassCtrl', ($scope, $auth, $timeout, $state) ->
  $scope.changeForm = {}
  $scope.submit = () ->
    $scope.success = false
    $scope.error = false
    $auth.updatePassword($scope.changeForm)
  $scope.$on 'auth:password-change-success', (ev, message) ->
    $scope.success = 'Password change successful!'
    $timeout((-> $state.go 'home'), 2000)
  $scope.$on 'auth:password-change-error', (ev, reason) ->
    $scope.error = reason.errors.full_messages[0]
