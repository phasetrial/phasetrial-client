'use strict'
angular.module('phasetrial')
.controller 'MainCtrl', ($scope, $state, $rootScope, $auth) ->
  $scope.user = $rootScope.user
  $scope.helloText = 'Welcome to the PhaseTrial Alpha'
  $scope.descriptionText = 'A brand new EDC system built using the latest web technologies.'
  $scope.signOut = () ->
    $auth.signOut()
    $scope.user = null
  $scope.open = false
  $scope.openDropdown = () ->
    $scope.open = !$scope.open
  $rootScope.$state = $state
  return
