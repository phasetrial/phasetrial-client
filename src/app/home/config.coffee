angular.module 'phasetrial'
  .factory "Config", (RailsResource) ->
    class Config extends RailsResource
      @configure url: "/api/configs", name: "config"
  .controller 'ConfigCtrl', ($scope, Config) ->
    Config.query().then (configs) -> $scope.configs = configs
