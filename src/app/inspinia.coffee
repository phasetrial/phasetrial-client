###*
# INSPINIA - Responsive Admin Theme
# 2.3
#
# Custom scripts
###

$(document).ready ->
# Full height

  fix_height = ->
    heightWithoutNavbar = $('body > #wrapper').height() - 61
    $('.sidebard-panel').css 'min-height', heightWithoutNavbar + 'px'
    navbarHeigh = $('nav.navbar-default').height()
    wrapperHeigh = $('#page-wrapper').height()
    if navbarHeigh > wrapperHeigh
      $('#page-wrapper').css 'min-height', navbarHeigh + 'px'
    if navbarHeigh < wrapperHeigh
      $('#page-wrapper').css 'min-height', $(window).height() + 'px'
    if $('body').hasClass('fixed-nav')
      if navbarHeigh > wrapperHeigh
        $('#page-wrapper').css 'min-height', navbarHeigh - 60 + 'px'
      else
        $('#page-wrapper').css 'min-height', $(window).height() - 60 + 'px'
    return

  $(window).bind 'load resize scroll', ->
    if !$('body').hasClass('body-small')
      fix_height()
    return
  setTimeout ->
    fix_height()
    return
  return
# Minimalize menu when screen is less than 768px
$ ->
  $(window).bind 'load resize', ->
    if $(this).width() < 769
      $('body').addClass 'body-small'
    else
      $('body').removeClass 'body-small'
    return
  return
