angular.module 'phasetrial'
# SchemaForm: form array
.factory 'UserForm', (RoleMap) ->
  return [
    {
      key: 'email'
      validationMessage:
        'invalidEmail': 'Invalid email address'
      $validators:
        invalidEmail: (value) ->
          if value is undefined then return true
          if value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/) is null then return false
          return true
    }
    'name'
    {key: 'roleId', type: 'select', titleMap: RoleMap.getRoles([])}
    {key: 'locked', htmlClass: 'checkbox-primary'}
  ]
# SchemaForm: schema object
.factory 'UserSchema', () ->
  return {
    type: 'object'
    title: 'User'
    properties:
      email:
        type: 'string'
        title: 'Email Address'
      name:
        type: 'string'
        title: 'Name'
      roleId:
        type: 'number'
        title: 'Role'
      locked:
        type: 'boolean'
        title: 'Locked'
  }
# Column definitions for UI Grid.
.factory 'UserColumns', (uiGridConstants) ->
  return [
    {field: 'id', displayName: '', width: 32, cellTemplate: 'app/common/edit-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {field: 'email', displayName: 'Email Address'}
    {field: 'name'}
    {field: 'roleName', displayName: 'Role'}
    {field: 'confirmed?', displayName: 'Confirmed', width: 86, cellTemplate: 'app/common/boolean-cell.html', enableColumnMenu: false}
    {field: 'locked', displayName: 'Locked', width: 86, cellTemplate: 'app/common/boolean-cell.html', enableColumnMenu: false}
    {name: 'Delete', displayName: '', width: 32, cellTemplate: 'app/common/delete-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
  ]
# Resource for getting users table from database.
.factory 'UserResource', (railsResourceFactory) ->
  return railsResourceFactory {
    url: "/api/users"
    name: "user"
  }
# Factory to get existing sites as options for a new subject.
.factory 'RoleMap', (RoleResource) ->
  return {
    getRoles: (titleMap) ->
      RoleResource.query().then (data) ->
        angular.forEach data, (role) ->
          titleMap.push { value: role.id, name: role.name }
      return titleMap
  }
# Controller for adding new users.
.controller 'AddUserCtrl', ($scope, UserSchema, UserForm, UserResource, RoleResource, Useful, $auth, $uibModalInstance) ->
  $scope.model = {}
  $scope.schema = UserSchema
  $scope.form = UserForm
  $scope.save = ->
    pass = Useful.randPass(12)
    $auth.submitRegistration({email: $scope.model.email, password: pass, password_confirmation: pass}).then (resp) ->
      UserResource.get(resp.data.data.id).then (resrc) ->
        resrc = angular.extend resrc, $scope.model
        resrc.update()
        RoleResource.get($scope.model.roleId).then (role) ->
          $scope.model.roleName = role.name
          $uibModalInstance.close $scope.model
# Controller for editing existing users.
.controller 'EditUserCtrl', ($scope, UserSchema, UserForm, grid, row, UserResource, $uibModalInstance) ->
  $scope.schema = UserSchema
  $scope.form = UserForm
  $scope.grid = grid
  $scope.row = row
  $scope.model = angular.copy row.entity
  $scope.save = ->
    row.entity = angular.extend row.entity, $scope.model
    row.entity.update()
    $uibModalInstance.close row.entity
# Controller for deleting existing users
.controller 'DelUserCtrl', ($scope, UserSchema, row, UserResource, $uibModalInstance) ->
  $scope.schema = UserSchema
  $scope.name = row.entity.email
  $scope.delete = ->
    row.entity.delete()
    $uibModalInstance.close row.entity
# Base controller. Loads users into the grid and contains button functions.
.controller 'UsersCtrl', ($scope, UserColumns, $uibModal, UserResource, gridFactory, $window) ->
  $scope.addRow = () ->
    modal = $uibModal.open
      templateUrl: 'app/common/add-row-modal.html'
      controller: 'AddUserCtrl'
    modal.result.then (result) ->
      $scope.gridOptions.data.push result unless result is undefined
  $scope.editRow = (grid, row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/edit-row-modal.html'
      controller: 'EditUserCtrl'
      resolve:
        grid: ->
          grid
        row: ->
          row
  $scope.delRow = (row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/delete-row-modal.html'
      controller: 'DelUserCtrl'
      resolve:
        row: ->
          row
    modal.result.then (result) ->
      index = $scope.gridOptions.data.indexOf(result)
      $scope.gridOptions.data.splice index, 1
  $scope.gridOptions = gridFactory.build($scope, UserColumns, false, false)
  $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  angular.element($window).bind 'resize', ->
    $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  UserResource.query().then (data) -> $scope.gridOptions.data = data
