'use strict'
angular.module('phasetrial')
###############################
# Inspinia Directives
###############################
  
# Directive used to set metisMenu and minimalize button
.directive 'sideNavigation', ($timeout) ->
  {
    restrict: 'A'
    link: (scope, element) ->
# Call metsi to build when user signup
      scope.$watch 'authentication.user', ->
        $timeout ->
          element.metisMenu()
          return
        return
      return

  }
.directive 'minimalizaSidebar', ($timeout) ->
  {
    restrict: 'A'
    template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>'
    controller: ($scope, $element) ->

      $scope.minimalize = ->
        angular.element('body').toggleClass 'mini-navbar'
        if !angular.element('body').hasClass('mini-navbar') or angular.element('body').hasClass('body-small')
# Hide menu in order to smoothly turn on when maximize menu
          angular.element('#side-menu').hide()
          # For smoothly turn on menu
          $timeout (->
            angular.element('#side-menu').fadeIn 400
            return
          ), 200
        else
# Remove all inline style from jquery fadeIn function to reset menu state
          angular.element('#side-menu').removeAttr 'style'
        return

      return

  }
.directive 'iboxTools', ($timeout) ->
  {
    restrict: 'A'
    scope: true
    templateUrl: 'app/common/ibox_tools.html'
    controller: ($scope, $element) ->
# Function for collapse ibox

      $scope.showhide = ->
        ibox = $element.closest('div.ibox')
        icon = $element.find('i:first')
        content = ibox.find('div.ibox-content')
        content.slideToggle 200
        # Toggle icon from up to down
        icon.toggleClass('fa-chevron-up').toggleClass 'fa-chevron-down'
        ibox.toggleClass('').toggleClass 'border-bottom'
        $timeout (->
          ibox.resize()
          ibox.find('[id^=map-]').resize()
          return
        ), 50
        return

      # Function for close ibox

      $scope.closebox = ->
        ibox = $element.closest('div.ibox')
        ibox.remove()
        return

      return

  }
.directive 'iboxToolsFullScreen', ($timeout) ->
  {
    restrict: 'A'
    scope: true
    templateUrl: 'app/common/ibox_tools_full_screen.html'
    controller: ($scope, $element) ->
# Function for collapse ibox

      $scope.showhide = ->
        ibox = $element.closest('div.ibox')
        icon = $element.find('i:first')
        content = ibox.find('div.ibox-content')
        content.slideToggle 200
        # Toggle icon from up to down
        icon.toggleClass('fa-chevron-up').toggleClass 'fa-chevron-down'
        ibox.toggleClass('').toggleClass 'border-bottom'
        $timeout (->
          ibox.resize()
          ibox.find('[id^=map-]').resize()
          return
        ), 50
        return

      # Function for close ibox

      $scope.closebox = ->
        ibox = $element.closest('div.ibox')
        ibox.remove()
        return

      # Function for full screen

      $scope.fullscreen = ->
        ibox = $element.closest('div.ibox')
        button = $element.find('i.fa-expand')
        $('body').toggleClass 'fullscreen-ibox-mode'
        button.toggleClass('fa-expand').toggleClass 'fa-compress'
        ibox.toggleClass 'fullscreen'
        setTimeout (->
          $(window).trigger 'resize'
          return
        ), 100
        return

      return

  }
###############################
# PhaseTrial Directives
###############################

# Directive for setting a manual date string when a date is typed instead of selected from datepicker.
.directive 'manualDate', () ->
  return {
    link: (scope, element) ->
      scope.$watch () ->
        return element[0].value
      , (newValue) ->
        scope.form.manualDate = newValue
      scope.$watch 'form.inputDate', (newValue) ->
        if typeof newValue is "string"
          element[0].value = newValue
  }
# Directive for combining date and time inputs into a single string that is stored in the database.
# Also performs some initial validation.
.directive 'dateTimeBuilder', () ->
  return {
    link: (scope, element) ->
      scope.dateChecker = (date) ->
        return true if date is ""
        return false if isNaN(Date.parse(date)) or Date.parse(date) < -2208988800000
        return false if date.match(/([a-zA-Z]{3,}[ ][1-2](9|0)[0-9]{2})|(^[1-2](9|0)[0-9]{2}$)/) is null
        return true
      scope.$watchGroup ['form.inputDate', 'form.inputTime', 'form.manualDate'], (newValues) ->
        options = {year: 'numeric', month: 'short', day: 'numeric'}
        validDate = scope.dateChecker(newValues[2])
        if newValues[0]
          date = new Date(newValues[0])
          element[0].value = date.toLocaleDateString('en-US', options) + " " + newValues[1] if newValues[1]
          element[0].value = date.toLocaleDateString('en-US', options) if !newValues[1]
        else if newValues[1] isnt ""
          element[0].value = newValues[1] if !validDate
          element[0].value = newValues[2] + " " + newValues[1] if validDate
        else if newValues[1] is ""
          element[0].value = newValues[2] if validDate
          element[0].value = "" if !validDate
        scope.ngModel.$setDirty()
        scope.ngModel.$setTouched()
        scope.ngModel.$setViewValue element[0].value, 'change'
        scope.ngModel.$commitViewValue
        angular.element(element[0]).removeClass('ng-empty')
        angular.element(element[0]).addClass('ng-not-empty')
        scope.ngModel.$validate()
  }
