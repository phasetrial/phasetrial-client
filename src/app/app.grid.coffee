angular.module 'phasetrial'
# Factory for building grids with default settings, as well as optional drag and tree features.
.factory "gridFactory", () ->
  return {
    build: ($scope, columns, draggable, tree) ->
      dragTemplate = (dragBool) ->
        if dragBool
          return """
                 <div grid="grid" class="ui-grid-draggable-row" draggable="true">
                 <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name"
                 class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'custom\': true }" ui-grid-cell>
                 </div></div>
                 """
        else
          return """
                 <div grid="grid">
                 <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name"
                 class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'custom\': true }" ui-grid-cell>
                 </div></div>
                 """
      return {
        columnDefs: columns
        enableGridMenu: true
        rowTemplate: dragTemplate(draggable)
        onRegisterApi: (gridApi) ->
          $scope.gridApi = gridApi
          if draggable
            gridApi.draggableRows.on.rowDragged $scope, (info) ->
              $scope.gridApi.core.setRowInvisible(row) for row in $scope.gridApi.grid.rows when row.entity is info.draggedRowEntity
            gridApi.draggableRows.on.rowFinishDrag $scope, ->
              $scope.gridApi.core.clearRowInvisible(row) for row in $scope.gridApi.grid.rows
            gridApi.draggableRows.on.rowDropped $scope, (info) ->
              drag = info.draggedRowEntity
              # If the table contains a sequence variable, then allow on the fly resorting to update 'seq'
              if drag.seq?
                target = info.targetRowEntity
                pos = info.position
                dir = 'up' if drag.seq > target.seq
                dir = 'down' if drag.seq < target.seq
                data = $scope.gridOptions.data
                targetSeq = target.seq
                targetSeq++ if dir is 'up' and pos is 'below'
                targetSeq-- if dir is 'down' and pos is 'above'
                resrc.seq++ for resrc in data when resrc.seq >= targetSeq and resrc.seq < drag.seq if dir is 'up'
                resrc.seq-- for resrc in data when resrc.seq > drag.seq and resrc.seq <= targetSeq if dir is 'down'
                data[data.indexOf(drag)].seq = targetSeq
                resrc.update() for resrc in data
#                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT)
          if tree
            $scope.gridApi.treeBase.on.rowExpanded $scope, (row) ->
              unless row.expanded
                forms = row.entity.formAppearances.sort (a, b) ->
                  aSeq = a.seq
                  bSeq = b.seq
                  return bSeq - aSeq
                angular.forEach forms, (form) ->
                  $scope.gridOptions.data.splice($scope.gridOptions.data.indexOf(row.entity) + 1, 0, form)
                  $scope.gridOptions.data[$scope.gridOptions.data.indexOf(form)].name = form.formName
                row.expanded = true
      }
#   Simple function for setting the grid height when window is resized
    getGridHeight: (windowHeight, offset) ->
      gridHeight = windowHeight - offset
      return gridHeight + 'px'
  }
  
