angular.module 'phasetrial'
# SchemaForm: form array
.constant 'SiteForm', [
  'number'
  'address'
  'timezone'
]
# SchemaForm: schema object
.factory 'SiteSchema', (TimeZones) ->
  return {
    type: 'object'
    title: 'Site'
    properties:
      number:
        type: 'string'
        title: 'Number'
      address:
        type: 'object'
        title: 'Address'
        properties:
          street:
            type: 'string'
            title: 'Street'
          city:
            type: 'string'
            title: 'City'
          state:
            type: 'string'
            title: 'State'
          zip:
            type: 'string'
            title: 'Zip'
      timezone:
        type: 'string'
        title: 'Timezone'
        enum: TimeZones
  }
# Column definitions for UI Grid.
.factory 'SiteColumns', (uiGridConstants) ->
  return [
    {field: 'id', displayName: '', width: 32, cellTemplate: 'app/common/edit-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {field: 'number', width: 100}
    {field: 'address.street', displayName: 'Address'}
    {field: 'address.city', name: 'City', width: 150}
    {field: 'address.state', name: 'State', width: 75}
    {field: 'address.zip', name: 'Zipcode', width: 75}
    {field: 'timezone', name: 'Timezone', width: 300}
    {name: 'Delete', displayName: '', width: 32, cellTemplate: 'app/common/delete-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
  ]
# Resource for getting sites table from database.
.factory 'SiteResource', (railsResourceFactory) ->
  return railsResourceFactory {
    url: "/api/sites"
    name: "site"
  }
# Controller for adding new sites.
.controller 'AddSiteCtrl', ($scope, SiteSchema, SiteForm, SiteResource, $uibModalInstance) ->
  $scope.model = {}
  $scope.schema = SiteSchema
  $scope.form = SiteForm
  $scope.save = ->
    new SiteResource($scope.model).create()
    $uibModalInstance.close $scope.model
# Controller for editing existing sites.
.controller 'EditSiteCtrl', ($scope, SiteSchema, SiteForm, grid, row, SiteResource, $uibModalInstance) ->
  $scope.schema = SiteSchema
  $scope.form = SiteForm
  $scope.grid = grid
  $scope.row = row
  $scope.model = angular.copy row.entity
  $scope.save = ->
    row.entity = angular.extend row.entity, $scope.model
    SiteResource.get(row.entity.id).then (resrc) ->
      resrc = angular.extend row.entity
      resrc.update()
    $uibModalInstance.close row.entity
# Controller for deleting existing sites
.controller 'DelSiteCtrl', ($scope, SiteSchema, row, SiteResource, $uibModalInstance) ->
  $scope.schema = SiteSchema
  $scope.name = row.entity.name
  $scope.delete = ->
    SiteResource.get(row.entity.id).then (resrc) -> resrc.delete()
    $uibModalInstance.close row.entity
# Base controller. Loads sites into the grid and contains button functions.
.controller 'SitesCtrl', ($scope, SiteColumns, $uibModal, SiteResource, gridFactory, $window) ->
  $scope.addRow = () ->
    modal = $uibModal.open
      templateUrl: 'app/common/add-row-modal.html'
      controller: 'AddSiteCtrl'
    modal.result.then (result) ->
      $scope.gridOptions.data.push result unless result is undefined
  $scope.editRow = (grid, row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/edit-row-modal.html'
      controller: 'EditSiteCtrl'
      resolve:
        grid: ->
          grid
        row: ->
          row
  $scope.delRow = (row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/delete-row-modal.html'
      controller: 'DelSiteCtrl'
      resolve:
        row: ->
          row
    modal.result.then (result) ->
      index = $scope.gridOptions.data.indexOf(result.data)
      $scope.gridOptions.data.splice index, 1
  $scope.gridOptions = gridFactory.build($scope, SiteColumns, false, false)
  $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  angular.element($window).bind 'resize', ->
    $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  SiteResource.query().then (data) -> $scope.gridOptions.data = data
