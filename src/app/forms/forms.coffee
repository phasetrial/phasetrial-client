angular.module 'phasetrial'
# SchemaForm: form array
.constant 'FormForm', [
  'name'
  'varname'
  'desc'
  { key: 'log', htmlClass: 'checkbox-primary' }
]
# SchemaForm: schema object
.constant 'FormSchema', {
  type: 'object'
  title: 'Form'
  properties:
    name:
      type: 'string'
      title: 'Name'
    varname:
      type: 'string'
      title: 'VarName'
    desc:
      type: 'string'
      title: 'Description'
    log:
      type: 'boolean'
      title: 'Log Form?'
}
# Column definitions for UI Grid.
.factory 'FormColumns', (uiGridConstants) ->
  return [
    {field: 'id', displayName: '', width: 32, cellTemplate: 'app/common/edit-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {field: 'appr', displayName: '', width: 32, cellTemplate: 'app/common/appearances-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {name: 'Preview', displayName: '', width: 32, cellTemplate: 'app/common/preview-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {field: 'name', width: 150, sort: { direction: uiGridConstants.ASC }, priority: 2}
    {field: 'varname', displayName: 'VarName', width: 150}
    {field: 'desc', name: 'Description'}
    {name: 'Delete', displayName: '', width: 32, cellTemplate: 'app/common/delete-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
  ]
# Resource for getting form_types table from database.
.factory 'FormTypeResource', (railsResourceFactory) ->
  return railsResourceFactory {
    url: "/api/form_types"
    name: "form_type"
  }
# Resource for getting variable_appearances table from database.
.factory 'VarAppearanceResource', (railsResourceFactory) ->
  return railsResourceFactory {
    url: "/api/variable_appearances"
    name: "variable_appearance"
  }
# Controller for adding new form types.
.controller 'AddFormCtrl', ($scope, FormSchema, FormForm, FormTypeResource, $uibModalInstance) ->
  $scope.model = {}
  $scope.schema = FormSchema
  $scope.form = FormForm
  $scope.save = ->
    new FormTypeResource($scope.model).create()
    $uibModalInstance.close $scope.model
# Controller for editing existing form types.
.controller 'EditFormCtrl', ($scope, FormSchema, FormForm, grid, row, FormTypeResource, $uibModalInstance) ->
  $scope.schema = FormSchema
  $scope.form = FormForm
  $scope.grid = grid
  $scope.row = row
  $scope.model = angular.copy row.entity
  $scope.save = ->
    row.entity = angular.extend row.entity, $scope.model
    FormTypeResource.get(row.entity.id).then (resrc) ->
      resrc = angular.extend row.entity
      resrc.update()
    $uibModalInstance.close row.entity
# Controller for deleting existing form types.
.controller 'DelFormCtrl', ($scope, FormSchema, row, FormTypeResource, $uibModalInstance) ->
  $scope.schema = FormSchema
  $scope.name = row.entity.name
  $scope.delete = ->
    FormTypeResource.get(row.entity.id).then (resrc) -> resrc.delete()
    $uibModalInstance.close row.entity
# Controller for setting which variables appear on a form type.
.controller 'VarApprCtrl', ($scope, row, VarResource, VarAppearanceResource, $uibModal, $uibModalInstance) ->
  $scope.form = row.entity
  $scope.appearances = []
  $scope.variables = []
  $scope.newAppr = []
  $scope.delAppr = []
  $scope.sortableOptions = {}
  $scope.selected = {}
  $scope.selected.vars = []
  VarResource.query().then (data) ->
    $scope.variables = data
    angular.forEach $scope.variables, (variable) ->
      angular.forEach variable.variableAppearances, (appr) ->
        $scope.appearances.push variable if appr.formTypeId is $scope.form.id
    angular.forEach $scope.appearances, (variable) ->
      $scope.variables.splice($scope.variables.indexOf(variable), 1)
    $scope.appearances.sort (a, b) ->
      aAppr = a.variableAppearances.filter (obj) ->
        return obj.formTypeId is $scope.form.id
      bAppr = b.variableAppearances.filter (obj) ->
        return obj.formTypeId is $scope.form.id
      return aAppr[0].seq - bAppr[0].seq
  $scope.addSelected = ->
    angular.forEach $scope.selected.vars, (variable) ->
      $scope.newAppr.push variable
      $scope.appearances.push variable
      $scope.variables.splice($scope.variables.indexOf(variable), 1)
    $scope.selected.vars = []
  $scope.delete = (variable) ->
    if $scope.newAppr.indexOf(variable) > -1
      $scope.newAppr.splice($scope.newAppr.indexOf(variable), 1)
    else
      $scope.delAppr.push variable
    $scope.appearances.splice($scope.appearances.indexOf(variable), 1)
  $scope.addVar = () ->
    secondModal = $uibModal.open
      templateUrl: 'app/common/add-row-modal.html'
      controller: 'AddVarCtrl'
    secondModal.result.then (result) ->
      $scope.variables.push result
  $scope.save = ->
    angular.forEach $scope.newAppr, (variable) ->
      new VarAppearanceResource({formTypeId: $scope.form.id, variableId: variable.id, seq: ($scope.appearances.indexOf(variable) + 1)}).create()
    angular.forEach $scope.delAppr, (variable) ->
      VarAppearanceResource.$get('/api/variable_appearances/find', {formTypeId: $scope.form.id, variableId: variable.id}).then (resrc) -> resrc[0].delete()
    angular.forEach $scope.appearances, (variable, apprIndex) ->
      VarAppearanceResource.$get('/api/variable_appearances/find', {formTypeId: $scope.form.id, variableId: variable.id}).then (match) ->
        VarAppearanceResource.get(match[0].id).then (resrc) ->
          resrc.seq = apprIndex + 1
          resrc.update()
    $uibModalInstance.close $scope.appearances
# Controller for previewing form layout.
.controller 'PreviewFormCtrl', ($scope, row) ->
  $scope.preview = true
  $scope.model = {}
  $scope.title = row.entity.name
  $scope.variables = row.entity.variableAppearances.sort (a, b) -> return a.seq - b.seq
  $scope.schema = {
    type: 'object'
    title: $scope.title
    properties: {}
  }
  $scope.form = []
  angular.forEach $scope.variables, (variable) ->
    $scope.schema.properties[variable.varname] =
      type: switch variable.datatype
        when 'integer', 'float' then 'number'
        when 'text', 'radio', 'datetime' then 'string'
        when 'checkbox' then 'array'
      title: variable.label
    if variable.datatype is 'radio'
      $scope.schema.properties[variable.varname].enum = variable.options
    if variable.datatype is 'checkbox'
      $scope.schema.properties[variable.varname].items = { type: 'string', enum: variable.options }
    formObj = { key: variable.varname }
    if variable.datatype is 'datetime'
      formObj.type = 'datepicker'
      formObj.format = 'dd MMM yyyy'
      if variable.options[0] is 'date' or variable.options[1] is 'date'
        formObj.date = true
        formObj.inputDate = null
        formObj.manualDate = ""
      if variable.options[0] is 'time' or variable.options[1] is 'time'
        formObj.time = true
        formObj.inputTime = ""
      formObj.$validators =
        invalidDate: (value) ->
          if formObj.manualDate is "" then return true
          if !isNaN(Date.parse(value)) and Date.parse(value) >= -2208988800000 then return true
          return false
        invalidTime: (value) ->
          if formObj.inputTime is "" then return true
          if formObj.inputTime.match(/(^[0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/) is null then return false
          return true
      formObj.opened = false
      formObj.open = () ->
        formObj.opened = !formObj.opened
    if variable.datatype is 'checkbox'
      formObj.htmlClass = 'checkbox-primary'
    $scope.form.push formObj
# Base controller. Loads form types into the grid and contains button functions.
.controller 'FormsCtrl', ($scope, FormColumns, $uibModal, FormTypeResource, gridFactory, $window) ->
  $scope.addRow = () ->
    modal = $uibModal.open
      templateUrl: 'app/common/add-row-modal.html'
      controller: 'AddFormCtrl'
    modal.result.then (result) ->
      $scope.gridOptions.data.push result unless result is undefined
  $scope.editRow = (grid, row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/edit-row-modal.html'
      controller: 'EditFormCtrl'
      resolve:
        grid: ->
          grid
        row: ->
          row
  $scope.delRow = (row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/delete-row-modal.html'
      controller: 'DelFormCtrl'
      resolve:
        row: ->
          row
    modal.result.then (result) ->
      index = $scope.gridOptions.data.indexOf(result.data)
      $scope.gridOptions.data.splice index, 1
  $scope.appearance = (row) ->
    modal = $uibModal.open
      templateUrl: 'app/forms/add-variables-modal.html'
      controller: 'VarApprCtrl'
      resolve:
        row: ->
          row
    modal.result.then (result) ->
      row.entity.variableAppearances = result
  $scope.preview = (row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/open-form-modal.html'
      controller: 'PreviewFormCtrl'
      resolve:
        row: ->
          row
  $scope.gridOptions = gridFactory.build($scope, FormColumns, false, false)
  $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  angular.element($window).bind 'resize', ->
    $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  FormTypeResource.query().then (data) -> $scope.gridOptions.data = data
