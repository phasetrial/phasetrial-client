angular.module 'phasetrial'
# SchemaForm: form array
.constant 'VarForm', [
  'name'
  'varname'
  { key: 'datatype', type: 'select', titleMap: [
    { value: 'integer', name: 'Integer' }
    { value: 'float', name: 'Float' }
    { value: 'text', name: 'Text' }
    { value: 'datetime', name: 'Date / Time' }
    { value: 'radio', name: 'Radio' }
    { value: 'checkbox', name: 'Checkbox' }
  ]}
  { key: 'options', type: 'array-custom', condition: "model.datatype === 'radio' || model.datatype === 'checkbox'" }
  { key: 'options', type: 'checkboxes-inline', condition: "model.datatype === 'datetime'", htmlClass: 'checkbox-primary', titleMap: [
    { value: 'date', name: 'Date' }
    { value: 'time', name: 'Time' }
  ]}
  { key: 'length', condition: "model.datatype === 'integer' || model.datatype === 'float' || model.datatype === 'text'"}
  { key: 'precision', condition: "model.datatype === 'float'" }
  { key: 'unknown', type: 'checkboxes-inline', condition: "model.datatype === 'datetime'", htmlClass: 'checkbox-primary' }
  { key: 'label', type: 'textarea'}
  { key: 'required', htmlClass: 'checkbox-primary' }
]
# SchemaForm: schema object
.constant 'VarSchema', {
  type: 'object'
  title: 'Variable'
  properties:
    name:
      type: 'string'
      title: 'Name'
    varname:
      type: 'string'
      title: 'VarName'
    datatype:
      type: 'string'
      title: 'Data Type'
      enum: ['integer', 'float', 'text', 'datetime', 'radio', 'checkbox']
    options:
      type: 'array'
      title: 'Options'
      items:
        type: 'string'
        title: ''
    length:
      type: 'number'
      title: 'Length'
    precision:
      type: 'number'
      title: 'Precision'
    unknown:
      type: 'array'
      title: 'Allow Unknown?'
      items:
        type: 'string'
        enum: ['Day', 'Month', 'Year']
    required:
      type: 'boolean'
      title: 'Required?'
    label:
      type: 'string'
      title: 'Label'
      placeholder: 'Enter question that will display on the form'
}
# Column definitions for UI Grid.
.factory 'VarColumns', (uiGridConstants) ->
  return [
    {field: 'id', displayName: '', width: 32, cellTemplate: 'app/common/edit-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {field: 'name', width: 150, sort: { direction: uiGridConstants.ASC }, priority: 2}
    {field: 'varname', displayName: 'VarName', width: 150}
    {field: 'datatype', name: 'Data Type', width: 150}
    {field: 'label', name: 'Label'}
    {name: 'Delete', displayName: '', width: 32, cellTemplate: 'app/common/delete-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
  ]
# Resource for getting variables table from database.
.factory 'VarResource', (railsResourceFactory) ->
  return railsResourceFactory {
    url: "/api/variables"
    name: "variable"
  }
# Controller for adding new variables.
.controller 'AddVarCtrl', ($scope, VarSchema, VarForm, VarResource, $uibModalInstance) ->
  $scope.model = {}
  $scope.schema = VarSchema
  $scope.form = VarForm
  $scope.save = ->
    result = new VarResource($scope.model).create()
    $uibModalInstance.close result
# Controller for editing existing variables.
.controller 'EditVarCtrl', ($scope, VarSchema, VarForm, grid, row, VarResource, $uibModalInstance) ->
  $scope.schema = VarSchema
  $scope.form = VarForm
  $scope.grid = grid
  $scope.row = row
  $scope.model = angular.copy row.entity
  $scope.save = ->
    row.entity = angular.extend row.entity, $scope.model
    VarResource.get(row.entity.id).then (resrc) ->
      resrc = angular.extend row.entity
      resrc.update()
    $uibModalInstance.close row.entity
# Controller for deleting existing variables.
.controller 'DelVarCtrl', ($scope, VarSchema, row, VarResource, $uibModalInstance) ->
  $scope.schema = VarSchema
  $scope.name = row.entity.name
  $scope.delete = ->
    VarResource.get(row.entity.id).then (resrc) -> resrc.delete()
    $uibModalInstance.close row.entity
# Base controller. Loads variables into the grid and contains button functions.
.controller 'VarsCtrl', ($scope, VarColumns, $uibModal, VarResource, gridFactory, $window) ->
  $scope.addRow = () ->
    modal = $uibModal.open
      templateUrl: 'app/common/add-row-modal.html'
      controller: 'AddVarCtrl'
    modal.result.then (result) ->
      $scope.gridOptions.data.push result unless result is undefined
  $scope.editRow = (grid, row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/edit-row-modal.html'
      controller: 'EditVarCtrl'
      resolve:
        grid: ->
          grid
        row: ->
          row
  $scope.delRow = (row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/delete-row-modal.html'
      controller: 'DelVarCtrl'
      resolve:
        row: ->
          row
    modal.result.then (result) ->
      index = $scope.gridOptions.data.indexOf(result.data)
      $scope.gridOptions.data.splice index, 1
  $scope.gridOptions = gridFactory.build($scope, VarColumns, false, false)
  $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  angular.element($window).bind 'resize', ->
    $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  VarResource.query().then (data) -> $scope.gridOptions.data = data
