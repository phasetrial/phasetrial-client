angular.module 'phasetrial'
# SchemaForm: form array
.constant 'VisitForm', ['name', 'varname', 'desc']
# SchemaForm: schema object
.constant 'VisitSchema', {
  type: 'object'
  title: 'Visit'
  properties:
    name:
      type: 'string'
      title: 'Name'
    varname:
      type: 'string'
      title: 'VarName'
    desc:
      type: 'string'
      title: 'Description'
}
# Column definitions for UI Grid.
.factory 'VisitColumns', (uiGridConstants) ->
  return [
    {field: 'id', displayName: '', width: 32, cellTemplate: 'app/common/edit-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {field: 'appr', displayName: '', width: 32, cellTemplate: 'app/common/appearances-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
    {field: 'seq', sort: { direction: uiGridConstants.ASC }, priority: 2, width: 40, visible: false}
    {field: 'name', width: 150}
    {field: 'varname', displayName: 'VarName', width: 150}
    {field: 'desc', name: 'Description'}
    {name: 'Delete', displayName: '', width: 32, cellTemplate: 'app/common/delete-row-button.html', headerCellTemplate: 'app/common/header-cell-no-sort.html', enableSorting: false, enableHiding: false}
  ]
# Resource for getting visits table from database.
.factory 'VisitResource', (railsResourceFactory) ->
  return railsResourceFactory {
    url: "/api/visits"
    name: "visit"
  }
# Resource for getting form_appearances table from database.
.factory 'FormAppearanceResource', (railsResourceFactory) ->
  return railsResourceFactory {
    url: "/api/form_appearances"
    name: "form_appearance"
  }
# Controller for adding new visits.
.controller 'AddVisitCtrl', ($scope, VisitSchema, VisitForm, VisitResource, $uibModalInstance) ->
  $scope.model = {}
  $scope.schema = VisitSchema
  $scope.form = VisitForm
  $scope.save = ->
    VisitResource.query().then (data) ->
      $scope.model.seq = data.length + 1
      new VisitResource($scope.model).create()
      $uibModalInstance.close $scope.model
# Controller for editing existing visits.
.controller 'EditVisitCtrl', ($scope, VisitSchema, VisitForm, grid, row, VisitResource, $uibModalInstance) ->
  $scope.schema = VisitSchema
  $scope.form = VisitForm
  $scope.grid = grid
  $scope.row = row
  $scope.model = angular.copy row.entity
  $scope.save = ->
    row.entity = angular.extend row.entity, $scope.model
    VisitResource.get(row.entity.id).then (resrc) ->
      resrc = angular.extend row.entity
      resrc.update()
    $uibModalInstance.close row.entity
# Controller for deleting existing visits.
.controller 'DelVisitCtrl', ($scope, VisitSchema, row, VisitResource, $uibModalInstance) ->
  $scope.schema = VisitSchema
  $scope.name = row.entity.name
  $scope.delete = ->
    VisitResource.get(row.entity.id).then (resrc) -> resrc.delete()
    $uibModalInstance.close row.entity
# Controller for setting which forms appear in a visit.
.controller 'FormApprCtrl', ($scope, row, FormTypeResource, FormAppearanceResource, $uibModal, $uibModalInstance) ->
  $scope.visit = row.entity
  $scope.appearances = []
  $scope.forms = []
  $scope.newAppr = []
  $scope.delAppr = []
  $scope.sortableOptions = {}
  $scope.selected = {}
  $scope.selected.forms = []
  FormTypeResource.query().then (data) ->
    $scope.forms = data
    angular.forEach $scope.forms, (form) ->
      angular.forEach form.formAppearances, (appr) ->
        $scope.appearances.push form if appr.visitId is $scope.visit.id
    angular.forEach $scope.appearances, (form) ->
      $scope.forms.splice($scope.forms.indexOf(form), 1)
    $scope.appearances.sort (a, b) ->
      aAppr = a.formAppearances.filter (obj) ->
        return obj.visitId is $scope.visit.id
      bAppr = b.formAppearances.filter (obj) ->
        return obj.visitId is $scope.visit.id
      return aAppr[0].seq - bAppr[0].seq
  $scope.addSelected = ->
    angular.forEach $scope.selected.forms, (form) ->
      $scope.newAppr.push form
      $scope.appearances.push form
      $scope.forms.splice($scope.forms.indexOf(form), 1)
    $scope.selected.forms = []
  $scope.delete = (form) ->
    if $scope.newAppr.indexOf(form) > -1
      $scope.newAppr.splice($scope.newAppr.indexOf(form), 1)
    else
      $scope.delAppr.push form
    $scope.appearances.splice($scope.appearances.indexOf(form), 1)
  $scope.addForm = () ->
    secondModal = $uibModal.open
      templateUrl: 'app/common/add-row-modal.html'
      controller: 'AddFormCtrl'
    secondModal.result.then (result) ->
      $scope.forms.push result
  $scope.save = ->
    angular.forEach $scope.newAppr, (form) ->
      new FormAppearanceResource({visitId: $scope.visit.id, formTypeId: form.id, seq: ($scope.appearances.indexOf(form) + 1)}).create()
    angular.forEach $scope.delAppr, (form) ->
      FormAppearanceResource.$get('/api/form_appearances/find', {visitId: $scope.visit.id, formTypeId: form.id}).then (resrc) -> resrc[0].delete()
    angular.forEach $scope.appearances, (form, apprIndex) ->
      FormAppearanceResource.$get('/api/form_appearances/find', {visitId: $scope.visit.id, formTypeId: form.id}).then (match) ->
        FormAppearanceResource.get(match[0].id).then (resrc) ->
          resrc.seq = apprIndex + 1
          resrc.update()
    $uibModalInstance.close $scope.appearances
# Base controller. Loads visits into the grid and contains button functions.
.controller 'VisitsCtrl', ($scope, VisitColumns, $uibModal, VisitResource, gridFactory, $window) ->
  $scope.addRow = () ->
    modal = $uibModal.open
      templateUrl: 'app/common/add-row-modal.html'
      controller: 'AddVisitCtrl'
    modal.result.then (result) ->
      $scope.gridOptions.data.push result unless result is undefined
  $scope.editRow = (grid, row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/edit-row-modal.html'
      controller: 'EditVisitCtrl'
      resolve:
        grid: ->
          grid
        row: ->
          row
  $scope.delRow = (row) ->
    modal = $uibModal.open
      templateUrl: 'app/common/delete-row-modal.html'
      controller: 'DelVisitCtrl'
      resolve:
        row: ->
          row
    modal.result.then (result) ->
      index = $scope.gridOptions.data.indexOf(result.data)
      $scope.gridOptions.data.splice index, 1
  $scope.appearance = (row) ->
    modal = $uibModal.open
      templateUrl: 'app/visits/form-appearances-modal.html'
      controller: 'FormApprCtrl'
      resolve:
        row: ->
          row
  $scope.gridOptions = gridFactory.build($scope, VisitColumns, true, false)
  $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  angular.element($window).bind 'resize', ->
    $scope.gridHeight = gridFactory.getGridHeight($window.innerHeight, 280)
  VisitResource.query().then (data) -> $scope.gridOptions.data = data
