# Getting started

Here are some learning resources to get you started with JavaScript/AngularJS:  

### Udemy
[JavaScript: Understanding the Weird Parts](https://www.udemy.com/understand-javascript/learn/v4/overview)  
[Learn and Understand AngularJS](https://www.udemy.com/learn-angularjs/learn/v4/overview)  
[Unit Testing AngularJS](https://www.udemy.com/unit-testing-angularjs/learn/v4/overview)

### Codeacademy
[Git](https://www.codecademy.com/learn/learn-git)  
[HTML & CSS](https://www.codecademy.com/learn/web)  
[JavaScript](https://www.codecademy.com/learn/javascript)  
[AngularJS](https://www.codecademy.com/learn/learn-angularjs)  

And another great git tutorial: [Atlassian](https://www.atlassian.com/git/tutorials)

-------------

# Libraries & Components

Here's a description of all the libraries, tools, and components that go into the frontend of PhaseTrial. If anything is confusing feel free to ask me about it.

### Pre-processors

I've been using Jade, LESS, and CoffeeScript in this project. They are basically different versions of HTML, CSS, and JavaScript with more readable syntax and less boilerplate. You can read up on them and play around with them here:

**HTML:** [Jade](http://jade-lang.com/) - [Converter](http://html2jade.org/)  
**CSS:** [LESS](http://lesscss.org/) - [Converter](http://lesscss.org/less-preview/)  
**JavaScript:** [CoffeeScript](http://coffeescript.org/) - [Converter](http://js2.coffee/)  

### Build Tools

###### [Gulp](http://gulpjs.com/) 

Gulp.js is a streaming build system that lets you define a bunch of tasks that you want to do to your code. For example, before we are able to use Jade, LESS, and CoffeeScript code, we have to convert it all to HTML, CSS, and JavaScript. Gulp does that automatically on the fly so you don't have to do it manually everytime you make a change. It also does things like "minify" the code, which is removing all the whitespace to make the filesize smaller.

The best thing it does is run a server for you locally, and it refreshes the page for you everytime you change the code, so you can instantly try out your changes. You can start a serve by typing `gulp serve:full`

You can find more info on the tasks Gulp can do [here](https://github.com/Swiip/generator-gulp-angular/blob/master/docs/user-guide.md). You can also look in the `gulp/` folder in the codebase to see the actual task definitions.

##### [NPM](https://www.npmjs.com/) & [Bower](http://bower.io/)

NPM and Bower are both just package managers. They download specific versions of the libraries I use in this project and store them in the `node_modules/` and `bower_components/` folders in the project. You'll notice that the GitLab repo doesn't contain these folders. That's because there's no need to carry the libraries along with the code everywhere. Once you have your own copy of the code (along with NPM and Bower installed on your machine), you can just run `npm install && bower install` and it will pull all the libraries down for you. The `package.json` and `bower.json` files in the root directory define the packages that need to be downloaded.

Why do we need two package managers? We don't, really. But it's nice to separate out the packages that are only used for building the code (like gulp and its components) and the packages that actually get sent down to the browser. NPM downloads the gulp components, and Bower downloads the libraries that will get packaged with our code.

### Libraries
###### [Bootstrap](http://getbootstrap.com/)
Bootstrap is a pretty popular library that has a ton of features, but the main ones I've been using are the grid system, button classes, and modals. The grid system lets you space content out evenly just by using some predefined CSS classes. For example, if I were to do the following:  

```
.row
  .col-lg-4
    p Column 1
  .col-lg-4
    p Column 2
  .col-lg-4
    p Column 3
```

The three pieces of text would be evenly spaced in thirds across the page. This comes in handy a lot more often than you would think, and it's a hell of a lot easier than playing around with CSS to get things spaced out just the way you want.

You can also apply classes like `.btn.btn-primary` to a div to turn it into a nice looking button. But there's a ton more you can do with Bootstrap, take a look at the documentation.

For the modal feature, I use the [UI Bootstrap](https://angular-ui.github.io/bootstrap/#modal) version, which is written in Angular rather than plain JavaScript. We also use UI Bootsraps's [datepicker](https://angular-ui.github.io/bootstrap/#datepicker).

###### [Font Awesome](http://fontawesome.io/)

Did you notice all the little icons next to the menu items and the buttons? We get those from Font Awesome! If you ever want to put a fancy icon somewhere on the page, you can just put `i.fa.fa-whatever` into your Jade and it'll show up as the icon you want. Check out the list of icons [here](http://fontawesome.io/icons/)!

###### [UI Router](https://github.com/angular-ui/ui-router/wiki)

UI Router is how I route to different pages in the application. It offers a few more features than the built-in Angular router, including the ability to name your states and structure them hierarchically. It also allows you to redirect easily in HTML using the `ui-sref` directive. Take a look at the link for a quick overview.

###### [UI Grid](http://ui-grid.info/docs/#/tutorial)

UI Grid is a really awesome library for creating grids. Since almost every single one of the pages has a grid on it, this one is crucial. You can take a look at the controllers, but basically I define the columns for each grid in a factory and then pass it into `GridFactory` (in app.grid.coffee) which creates the gridOptions object that allows the grid to function. Then gridOptions.data gets the data from the database and loads it into the grid. There are lots of little subcomponents like resizable columns, draggable rows, and tree view, they're all explained in the documentation as well.

###### [UI Sortable](https://github.com/angular-ui/ui-sortable)

This is a little addon that allows you to create a list of items that you can drag to resort. It's used in the modals where you define form and variable appearances.

###### [UI Select](http://angular-ui.github.io/ui-select/)

UI Select is a select box that offers multi-select and option searching. It is also used in the modals where you define form and variable appearances.

###### [Angular Schema Form](https://github.com/json-schema-form/angular-schema-form)

Angular Schema Form is another really important library. Normally with forms, you would have to write each and every field into the HTML, requiring you to know the fields in advance. With Angular Schema Form, you can define forms using JavaScript, allowing you to create forms on the fly. When we load a CRF, for example, we pull all the variables for that CRF from the database and use the information to populate the `$scope.schema` and `$scope.form` objects, which will create the form on the screen.

It also allows you to define custom add-ons - which I did for the "Date / Time" variable type, to allow the UI Bootstrap calendar and the clockpicker. We may look into adding more custom types later on.

Take a look at the documentation to see how this all works, and be sure to take a look at their [demo page](http://schemaform.io/examples/bootstrap-example.html).

###### [AngularJS Rails Resource](https://github.com/FineLinePrototyping/angularjs-rails-resource)

This library takes care of loading and saving the data from the Rails app for us. When we define a new resource, all we have to specify is the URL endpoint for the Rails app and the singular name, like so:

```
.factory 'FormTypeResource', (railsResourceFactory) ->
  return railsResourceFactory {
    url: "/api/form_types"
    name: "form_type"
  }
```

You can load a table by calling `FormTypeResource.query()`. When you have a resource object in your JavaScript, you can just call `.update()` or `.delete()` and it will do all the API calls for you!

###### [Angular Token Auth](https://github.com/lynndylanhurley/ng-token-auth)

Angular Token Auth handles the user authentication at the frontend. It works with the [devise-token-auth](https://github.com/lynndylanhurley/devise_token_auth) gem in the Rails app to set up new users, do password resets, make sure users are logged in before accessing the app, etc. When you log in, it stores a token in your browser cookies so that it knows you're still logged in when you come back later on. You can also set expiration on the tokens so that there's an inactivity period.

###### [Angular Permission](https://github.com/Narzerus/angular-permission)

Angular Permission handles role-based access to certain areas of the app. Once we define our permissions, we can add a `data: permissions:` attribute to our router states and it will only allow users with those permissions to access that state. You can also add a `permission` and `permission-only` directive to any HTML element to hide it based on a user's permissions.

# Moving Forward

I know this is all probably a little overwhelming. But don't worry, you don't need to become a master of all these libraries and tools right away to be able to contribute. Just take a glance at what interests you, and go for a deep dive when you really need to know how a certain library works. And of course, I'm always here to help!
