'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')();

var wiredep = require('wiredep').stream;
var _ = require('lodash');

var browserSync = require('browser-sync');

gulp.task('inject-reload', ['inject'], function() {
  browserSync.reload();
});

gulp.task('inject', ['scripts', 'styles', 'markups'], function () {
  var styleSources = gulp.src('app/**/*.css', {'cwd': './.tmp/serve'});

  var scriptSources = gulp.src('app/**/*.js', {'cwd': './.tmp/serve'})
  .pipe($.angularFilesort()).on('error', conf.errorHandler('AngularFilesort'));

  var injectOptions = {
    addRootSlash: false
  };

  return gulp.src('serve/*.html', {'cwd': './.tmp'})
    .pipe($.inject(styleSources, injectOptions))
    .pipe($.inject(scriptSources, injectOptions))
    .pipe(wiredep(_.extend({}, conf.wiredep)))
    .pipe(gulp.dest('./.tmp/serve'));
});
