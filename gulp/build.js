'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');
var cleanCSS = require('gulp-clean-css');

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('partials', ['inject'], function () {
  return gulp.src([
    path.join(conf.paths.src, '/app/**/*.html'),
    path.join(conf.paths.tmp, '/serve/app/**/*.html')
  ])
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'phasetrial',
      root: 'app'
    }))
    .pipe(gulp.dest(conf.paths.tmp + '/partials/'));
});

gulp.task('html', ['partials'], function () {
  var partialsInjectFile = gulp.src('.tmp/partials/templateCacheHtml.js', { read: false });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: path.join(conf.paths.tmp, '/partials'),
    addRootSlash: false
  };

  var htmlFilter = $.filter('.tmp/**/*.html', { restore: true });
  var jsFilter = $.filter('.tmp/serve/**/*.js', { restore: true });
  var cssFilter = $.filter('.tmp/serve/**/*.css', { restore: true });
  // var assets;

  return gulp.src('.tmp/serve/index.html')
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe($.useref({searchPath:['./bower_components','./.tmp/serve','./.tmp/partials']}))
    // .pipe(assets = $.useref.assets({searchPath:['./bower_components','./.tmp/serve']}))
    .pipe(jsFilter)
    .pipe($.sourcemaps.init())
    .pipe($.ngAnnotate())
    .pipe($.uglify()).on('error', conf.errorHandler('Uglify'))
    .pipe($.sourcemaps.write('maps'))
    .pipe($.rev())
    .pipe(jsFilter.restore)
    .pipe(cssFilter)
    .pipe($.sourcemaps.init())
    .pipe($.replace('../../bower_components/bootstrap/fonts/', '../assets/fonts/'))
    .pipe($.replace('../../bower_components/font-awesome/fonts', '../assets/fonts'))
    .pipe($.replace('ui-grid.eot', '../assets/fonts/ui-grid.eot'))
    .pipe($.replace('ui-grid.woff', '../assets/fonts/ui-grid.woff'))
    .pipe($.replace('ui-grid.ttf', '../assets/fonts/ui-grid.ttf'))
    .pipe($.replace('ui-grid.svg', '../assets/fonts/ui-grid.svg'))
    .pipe(cleanCSS({keepSpecialComments : 0}))
    .pipe($.sourcemaps.write('maps'))
    .pipe($.rev())
    .pipe(cssFilter.restore)
    // .pipe(assets.restore())
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true,
      conditionals: true
    }))
    .pipe(htmlFilter.restore)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')))
    .pipe($.size({ title: path.join(conf.paths.dist, '/'), showFiles: true }));
  });

// Only applies for fonts from bower dependencies
// Custom fonts are handled by the "other" task
gulp.task('fonts', function () {
  return gulp.src($.mainBowerFiles())
    .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe($.flatten())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/assets/fonts/')));
});

gulp.task('fontawesome', function () {
  return gulp.src('bower_components/font-awesome/fonts/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest(conf.paths.dist + '/assets/fonts/'));
});

gulp.task('bootstrapfonts', function () {
  return gulp.src('bower_components/bootstrap/fonts/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest(conf.paths.dist + '/assets/fonts/'));
});

gulp.task('other', function () {
  var fileFilter = $.filter(function (file) {
    return file.stat.isFile();
  });

  return gulp.src([
    path.join(conf.paths.src, '/**/*'),
    path.join('!' + conf.paths.src, '/**/*.{html,css,js,less,coffee,jade}')
  ])
    .pipe(fileFilter)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('clean', function () {
  return $.del([path.join(conf.paths.dist, '/'), path.join(conf.paths.tmp, '/')]);
});

gulp.task('build', ['html', 'fonts', 'fontawesome', 'bootstrapfonts', 'other']);
