'use strict';

var gulp = require('gulp');
var exec = require('child_process').exec;

gulp.task('deploy', ['build'], function (cb) {
  exec('aws s3 cp dist/index.html s3://phasetrial/ --acl public-read --cache-control no-cache && aws s3 sync dist/scripts s3://phasetrial/scripts --delete --acl public-read --cache-control max-age=2592000,public && aws s3 sync dist/styles s3://phasetrial/styles --delete --acl public-read --cache-control max-age=2592000,public', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});
